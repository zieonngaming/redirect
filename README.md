# Redirect

A Chrome Extension that allows you to make custom link redirections.

Example: google.com -> startpage.com (NOTE: Any link with google.com will be changed though. https://www.google.com/thisisalink/forms will be put too https://www.startpage.com/thisisalink/forms)

![Redirect Options Page](redirect.png)
![Reddirect Popup Page](redirectpu.png)